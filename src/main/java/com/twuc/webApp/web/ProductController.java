package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ProductController {
    @Autowired
    ProductRepository repo;

    @PostMapping("/api/products")
    public ResponseEntity<Product> createProducts(@RequestBody @Valid CreateProductRequest productRequest) {
        Product product = repo.saveAndFlush(new Product(productRequest.getName(), productRequest.getPrice(), productRequest.getUnit()));
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("location", "http://localhost/api/products/" + product.getId())
                .build();
    }

    @GetMapping("/api/products/{productId}")
    public ResponseEntity<Product> getProducts(@PathVariable Long productId) {
        Optional<Product> optional = repo.findById(productId);
        if (optional.isPresent()) {
            Product product = optional.get();
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(new Product(product.getName(), product.getPrice(), product.getUnit()));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
