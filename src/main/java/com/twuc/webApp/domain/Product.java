package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, length = 64)
    private String name;
    @Column(nullable = false, length = 10000)
    private Integer price;
    @Column(nullable = false, length = 32)
    private String unit;

    public Product() {
    }

    public Product(String name, Integer price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Integer getPrice() {
        return this.price;
    }

    public String getUnit() {
        return this.unit;
    }
}
