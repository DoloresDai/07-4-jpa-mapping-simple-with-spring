package com.twuc.webApp.contract;

import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
public class CreateProductRequest {
    @NotNull
    @Size(min = 1, max = 64)
    private String name;

    @NotNull
    @Min(value = 1)
    @Max(value = 10000)
    private Integer price;

    @NotNull
    @Size(min = 1, max = 32)
    private String unit;

    public CreateProductRequest() {
    }

    public CreateProductRequest(
        @NotNull @Size(min = 1, max = 64) String name,
        @NotNull @Min(value = 1) @Max(value = 10000) Integer price,
        @NotNull @Size(min = 1, max = 32) String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}
